This document contains the sample parameters needed to evaluate the mat-file given in this folder.


Cells_in_buffer.mat:

layers below molecule: glass (n=1.52), then 2nm titanium, then 20nm gold
molecule's layer: buffer (n=1.4), quantum yield 0.9
layers above molecule: buffer (n=1.4)
general parameters: emission filter allows wavelengths in the range 645-800nm, emission spectrum "cells.txt" was measured using these filters
free space lifetime: 2ns
